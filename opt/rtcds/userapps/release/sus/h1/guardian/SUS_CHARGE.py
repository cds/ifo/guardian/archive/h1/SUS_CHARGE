from guardian import GuardState, GuardStateDecorator, NodeManager
from gpstime import gpsnow
from datetime import date
import time
import os
import sys
import cdsutils 
import lscparams

nominal = 'INJECTIONS_COMPLETE'
nodes_list = ['ESD_EXC_ITMX','ESD_EXC_ITMY','ESD_EXC_ETMX','ESD_EXC_ETMY']
nodes = NodeManager(nodes_list)

DATA_DIR = '/opt/rtcds/userapps/release/sus/common/scripts/quad/InLockChargeMeasurements/rec_LHO'

ifo_params = {}

standdown_sources = ['snews', 'fermi', 'swift']

#################################################################
#FUNCTIONS
class assert_IFO_inlock(GuardStateDecorator):
    def pre_exec(self):
        lock_state = ezca['GRD-ISC_LOCK_STATE_N'] 
        if lock_state < 600: 
            log('IFO is not Locked, Going to DOWN')
            return 'DOWN'

def checking_files_created():
    """
    Reads and prints the names of the files created in the last 10 minutes.
    """
    all_files = os.listdir(DATA_DIR)
    recent_data = 600 ##10 MIN
    gps_now = ezca['DAQ-DC0_GPS']
    for namedfile in all_files:
        if len(namedfile) == 25 and namedfile[-4:] == '.txt': 
            gps_file_times = float(namedfile[11:21])
            if gps_file_times >= gps_now - recent_data:
                log('Data saved in {}/{}'.format(DATA_DIR, namedfile))

def transition_ETMX_ESD_to_ITMX_ESD():
    #save values to be reverted later
    ifo_params['ITMX_L2L_gain'] = ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_GAIN']
    ifo_params['ITMX_L2L_tramp'] = ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_TRAMP']
    ifo_params['ETMX_L2L_tramp'] = ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_TRAMP']
    ifo_params['ITMX_BIAS_tramp'] = ezca['SUS-ITMX_L3_LOCK_BIAS_TRAMP']
    #transition from the Low noise ESD state locked on ETMX to ETMX L2/L1 +ITMX ESD.  
    ezca.get_LIGOFilter('SUS-ITMX_L3_DRIVEALIGN_L2L').ramp_gain(0, ramp_time=10, wait=True)	
    ezca.get_LIGOFilter('SUS-ITMX_L3_LOCK_BIAS').ramp_gain(1, ramp_time=60, wait=True)
    ezca.get_LIGOFilter('SUS-ITMX_M0_LOCK_L').turn_off('INPUT')
    ezca['SUS-ITMX_L1_LOCK_OUTSW_L'] = 0
    ezca['SUS-ITMX_L2_LOCK_OUTSW_L'] = 0
    ezca['SUS-ITMX_L3_LOCK_OUTSW_L'] = 1
    ezca.get_LIGOFilter('SUS-ITMX_L3_DRIVEALIGN_L2L').only_on('INPUT', 'FM7', 'OUTPUT', 'DECIMATION') 
    ezca['LSC-ARM_OUTPUT_MTRX_3_1'] = 1
    # if we were using FF on ETMX we neeed to add swap for the MICH and SRCL FF output matrix
    ezca.get_LIGOFilter('SUS-ITMX_L3_LOCK_L').ramp_gain(1, ramp_time=10, wait=False)
    ezca.get_LIGOFilter('SUS-ITMX_L3_ISCINF_L').only_on('INPUT', 'FM5', 'FM8','FM9', 'FM10', 'OUTPUT', 'DECIMATION') 
    time.sleep(10)
    ezca.get_LIGOFilter('SUS-ITMX_L3_DRIVEALIGN_L2L').ramp_gain(-108.9, ramp_time=20, wait=False)
    ezca.get_LIGOFilter('SUS-ETMX_L3_DRIVEALIGN_L2L').ramp_gain(0, ramp_time=20, wait=True)
    log('Transitioned from the Low noise ESD state locked on ETMX to ETMX L2/L1 +ITMX ESD')

def transition_ITMX_ESD_to_ETMX_ESD():
    ##Section 1: is swapping ESD DARM control from ITMX to ETMX
    ezca.get_LIGOFilter('SUS-ITMX_L3_DRIVEALIGN_L2L').ramp_gain(0, ramp_time=20, wait=False)
    ezca.get_LIGOFilter('SUS-ETMX_L3_DRIVEALIGN_L2L').ramp_gain(lscparams.ETMX_GND_MIN_DriveAlign_gain, ramp_time=20, wait=False)
    time.sleep(20)
    ##Section 2: setting matrix and filters back
    ezca.get_LIGOFilter('SUS-ITMX_L3_DRIVEALIGN_L2L').only_on('INPUT', 'FM4', 'FM5', 'OUTPUT', 'DECIMATION')
    ezca.get_LIGOFilter('SUS-ITMX_L3_ISCINF_L').only_on('INPUT', 'OUTPUT', 'DECIMATION') 
    ezca['SUS-ITMX_L3_LOCK_L_GAIN']=0
    #Section 3: if we were using FF on ETMX we neeed to add swap for the MICH and SRCL FF output matrix
    ezca['LSC-ARM_OUTPUT_MTRX_3_1'] = 0 #DARM to ITMX off
    #Section 4: turning the ITMX bias back off
    ezca.get_LIGOFilter('SUS-ITMX_L3_LOCK_BIAS').ramp_gain(0, ramp_time=60, wait=True)
    ezca.get_LIGOFilter('SUS-ITMX_L3_DRIVEALIGN_L2L').ramp_gain(ifo_params['ITMX_L2L_gain'], ramp_time=10, wait=True)
    log('Transitioned from the low noise ESD state locked on ITMX to ETMX')
    ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_TRAMP'] = ifo_params['ITMX_L2L_tramp']
    ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_TRAMP'] = ifo_params['ETMX_L2L_tramp']
    ezca['SUS-ITMX_L3_LOCK_BIAS_TRAMP'] = ifo_params['ITMX_BIAS_tramp']

#################################################################
#STATES

class INIT(GuardState):
    def main(self):
        nodes.set_managed()
    def run(self):
        return 'DOWN'

class DOWN(GuardState): # State to stop all injections
    index = 2
    goto = True
    def main(self):
        for esd_quad in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
            nodes['ESD_EXC_{}'.format(esd_quad)] = 'DOWN'
        log('All nodes taken to DOWN, ISC_LOCK should have taken care of reverting settings.')
        ifo_params = {}
    @nodes.checker()
    def run(self):
        if nodes.arrived:
            return True


class WAITING(GuardState): # State to sit in most of the time
    index = 5
    def run(self):
        # Injections can run automatically if IFO is locked on Tuesdays @ 7:45am PST
        if date.today().weekday() == 1 and time.strftime("%H:%M:%S") == '07:45:00' and ezca['GRD-ISC_LOCK_STATE_N'] == 600:
            # Check for stand down alert
            if ezca['OPS-STANDDOWN_STATE'] != 0:
                if ezca['CAL-INJ_EXTTRIG_ALERT_SOURCE'].lower() in standdown_sources:
                    notify('Stand down alert active! Not running injections')
                    return False
                # Check if stand down is from KamLAND
                elif ezca['OPS-STANDDOWN_EVENT_NAME'].lower() == 'superkam':
                    notify('KamLAND stand down alert active! Not running injections')
                    return False
            else:
                return True
        if ezca['GRD-SUS_CHARGE_TARGET'] == 'PREP_MANUAL_INJECTIONS':
            return True

class PREP_MANUAL_INJECTIONS(GuardState):
    index = 7
    def run(self):
        notify('Request INJECTIONS COMPLETE to run injections')
        return True

##Doing ITMY,ITMY,ETMY
class FIRST_ESD_INJECTION(GuardState):
    index = 10
    request = False
    @assert_IFO_inlock
    def main(self):  
        for esd_EXC_quad in ['ITMX', 'ITMY', 'ETMY']:
            nodes['ESD_EXC_{}'.format(esd_EXC_quad)] = 'COMPLETE'
    @nodes.checker()
    @assert_IFO_inlock
    def run(self):
        if nodes.arrived:	
            checking_files_created()
            for esd_quad in ['ITMX', 'ITMY', 'ETMY']:
                nodes['ESD_EXC_{}'.format(esd_quad)] = 'DOWN'
            return True

class SWAP_TO_ITMX(GuardState):
    index = 11
    request = False
    @assert_IFO_inlock
    def main(self):    
        transition_ETMX_ESD_to_ITMX_ESD()     
        return True

##Doing ETMX
class SEC_ESD_INJECTION(GuardState):
    index = 12
    request = False
    @assert_IFO_inlock
    def main(self):        
        nodes['ESD_EXC_ETMX'] = 'COMPLETE'
    @nodes.checker() 
    @assert_IFO_inlock
    def run(self):
        if nodes.arrived:
            checking_files_created()
            nodes['ESD_EXC_ETMX'] = 'DOWN'
            return True

#SWAPPING BACK
class SWAP_BACK_ETMX(GuardState):
    index = 13
    request = False
    @assert_IFO_inlock
    def main(self):          
        transition_ITMX_ESD_to_ETMX_ESD()     
        return True

class INJECTIONS_COMPLETE(GuardState):
    index = 20
    def run(self):
        log("All injections complete, returning to DOWN")
        return 'DOWN'

#################################################################
edges = [
    ('DOWN','WAITING'),
    ('WAITING','PREP_MANUAL_INJECTIONS'),
    ('PREP_MANUAL_INJECTIONS', 'FIRST_ESD_INJECTION'),
    ('WAITING', 'FIRST_ESD_INJECTION'),
    ('FIRST_ESD_INJECTION','SWAP_TO_ITMX'), 
    ('SWAP_TO_ITMX','SEC_ESD_INJECTION'),
    ('SEC_ESD_INJECTION','SWAP_BACK_ETMX'),
    ('SWAP_BACK_ETMX','INJECTIONS_COMPLETE')
    ]
